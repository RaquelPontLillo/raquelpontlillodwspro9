<?php

use Illuminate\Database\Seeder;

class CursosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cursos')->insert([
        	['nombre' => 'Matematicas', 	'horas' => 70],
        	['nombre' => 'Lengua', 		'horas' => 50],
        	['nombre' => 'Ingles', 		'horas' => 25],
        	['nombre' => 'Biologia', 		'horas' => 50],
        	['nombre' => 'Quimica', 		'horas' => 60],
        	['nombre' => 'Geologia', 		'horas' => 35],
        	['nombre' => 'Historia', 		'horas' => 20],
        	['nombre' => 'Valenciano', 	'horas' => 30],
        	['nombre' => 'Fisica', 		'horas' => 40],
        	['nombre' => 'Dibujo', 		'horas' => 45]
        ]);
    }
}
