<?php

use Illuminate\Database\Seeder;

class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
        	['nombre' => 'Marcos', 		'curso_id' => 1],
        	['nombre' => 'Maria', 		'curso_id' => 2],
        	['nombre' => 'Marta', 		'curso_id' => 3],
        	['nombre' => 'Joaquin', 	'curso_id' => 4],
        	['nombre' => 'Vicente', 	'curso_id' => 5],
        	['nombre' => 'Luis', 		'curso_id' => 6],
        	['nombre' => 'Javier', 		'curso_id' => 7],
        	['nombre' => 'Jose', 		'curso_id' => 8],
        	['nombre' => 'Andreu', 		'curso_id' => 9],
        	['nombre' => 'Eva', 		'curso_id' => 10],
        	['nombre' => 'Herminia', 	'curso_id' => 1],
        	['nombre' => 'Miguel', 		'curso_id' => 2],
        	['nombre' => 'Teresa', 		'curso_id' => 3],
        	['nombre' => 'Lucia', 		'curso_id' => 4],
        	['nombre' => 'Sonia', 		'curso_id' => 5],
        	['nombre' => 'Victor', 		'curso_id' => 6],
        	['nombre' => 'Moises', 		'curso_id' => 7],
        	['nombre' => 'Raul', 		'curso_id' => 8],
        	['nombre' => 'Ruben', 		'curso_id' => 9],
        	['nombre' => 'Carmen', 		'curso_id' => 10]
        ]);
    }
}
