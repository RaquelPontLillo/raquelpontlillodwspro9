<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $tabla = 'alumnos';
    protected $fillable = ['nombre','curso_id'];
    public $timestamps = false;
    
    public function cursos() {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
}
