<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Curso;

class ControladorCurso extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = DB::table('cursos')->paginate(5);
        return view('readCursos')->with('cursos',$cursos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = $this->lastId();
        return view('createCurso')->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $curso = new Curso($request->all());
        $curso->save();
        flash('El curso '.$curso->nombre.' ha sido creado con éxito', 'success');
        return redirect()->route('cursos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        if (Curso::find($id)) {
            $cursos = array();
            $curso = Curso::find($id);
            array_push($cursos, $curso);
            return view('readCursos')->with('cursos',$cursos);
        } else {
            flash('No existen coincidencias con el ID seleccionado', 'danger');
            return redirect()->route('cursos');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Curso::find($id);
        return view('editCurso')->with('curso',$curso);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Curso::find($id)) {
            $curso  = Curso::find($id);
            $curso->nombre = $request->nombre;
            $curso->horas = $request->horas;
            $curso->save();
            flash('El curso '.$curso->nombre.' ha sido actualizado con éxito', 'success');
        } else {
            flash('El curso seleccionado no existe', 'danger');
        }
        return redirect()->route('cursos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso = Curso::find($id);
        $curso->delete();
        flash('El curso '.$curso->nombre.' ha sido borrado con éxito', 'success');
        return redirect()->route('cursos');
    }
    
    public function lastId()
    {
        $id = DB::select('SELECT MAX(id) AS id FROM cursos');
        $id = $id[0]->id;
        $id++;
        return $id;
    }
}
