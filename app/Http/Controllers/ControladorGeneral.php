<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControladorGeneral extends Controller
{

	protected $usuario = 'root';
	protected $password = 'pass';
   	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   	public function index()
   	{
   		return view('inicio');
   	}

   	public function login() {
   		return view('login');
   	}

   	public function autenticado(Request $request) {
   		if ($request->usuario == $this->usuario && $request->password == $this->password) {
   			$request->session()->put('auth', 'autenticado');
   			return redirect()->route('/');
   		} else {
   			flash('El usuario o la contraseña introducidos no existen o no coinciden entre ellos.','danger');
   			return redirect()->route('login');
   		}
   	}

   	public function logout() {
   		session()->forget('auth');
        return redirect()->route('login');
   	}
}