<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Curso;
use App\Alumno;

class ControladorAlumno extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos = Alumno::paginate(5);
        return view('readAlumnos')->with('alumnos',$alumnos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = $this->lastId();
        $cursos = Curso::all();
        return view('createAlumno')->with('id',$id)->with('cursos',$cursos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $alumno = new Alumno($request->all());
        $alumno->save();
        flash('El alumno '.$alumno->nombre.' ha sido creado con éxito', 'success');
        return redirect()->route('alumnos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        if (Alumno::find($id)) {
            $alumnos = array();
            $alumno = Alumno::find($id);
            array_push($alumnos, $alumno);
            return view('readAlumnos')->with('alumnos',$alumnos);
        } else {
            flash('No existen coincidencias con el ID seleccionado', 'danger');
            return redirect()->route('alumnos');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alumno = Alumno::find($id);
        $cursos = Curso::all();
        return view('editAlumno')->with('alumno',$alumno)->with('cursos',$cursos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Alumno::find($id)) {
            $alumno = Alumno::find($id);
            $alumno->nombre = $request->nombre;
            $alumno->curso_id = $request->curso;
            $alumno->save();
            flash('El alumno '.$request->nombre.' ha sido actualizado con éxito', 'success');
        } else {
            flash('El alumno seleccionado no existe', 'danger');
        }
        return redirect()->route('alumnos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alumno = Alumno::find($id);
        $alumno->delete();
        flash('El alumno '.$alumno->nombre.' ha sido borrado con éxito', 'success');
        return redirect()->route('alumnos');
    }
    
    public function lastId()
    {
        $id = DB::select("SELECT MAX(id) AS id FROM alumnos");
        $id = $id[0]->id;
        $id++;
        return $id;
    }
}
