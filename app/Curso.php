<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $tabla = 'cursos';
    protected $fillable = ['nombre','horas'];
    public $timestamps = false;

    public function alumnos() {
        return $this->hasMany('App\Alumno', 'id');
    }
}
