<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas generales
Route::get('/', [
    'as' => '/',
    'uses' => 'ControladorGeneral@index'
])->middleware("log");
Route::get('login', [
    'as' => 'login',
    'uses' =>'ControladorGeneral@login'
]);
Route::POST('autenticar', [
    'as' => 'autenticar',
    'uses' =>'ControladorGeneral@autenticado'
]);
Route::get('logout', [
    'as' => 'logout',
    'uses' =>'ControladorGeneral@logout'
])->middleware("log");

//Rutas de cursos
Route::get('cursos', [
    'as' => 'cursos',
    'uses' => 'ControladorCurso@index'
])->middleware("log");
Route::get('cursos/borrar/{id}', [
    'as' => 'cursos.destroy',
    'uses' => 'ControladorCurso@destroy'
])->middleware("log");
Route::get('cursos/editar/{id}', [
    'as' => 'cursos.edit',
    'uses' => 'ControladorCurso@edit'
])->middleware("log");
Route::get('cursos/crear', [
    'as' => 'cursos.create',
    'uses' => 'ControladorCurso@create'
])->middleware("log");
Route::get('cursos/guardar', [
    'as' => 'cursos.store',
    'uses' => 'ControladorCurso@store'
])->middleware("log");
Route::POST('cursos/actualizar/{id}', [
    'as' => 'cursos.update',
    'uses' => 'ControladorCurso@update'
])->middleware("log");
Route::get('cursos/buscar', [
    'as' => 'cursos.show',
    'uses' => 'ControladorCurso@show'
])->middleware("log");

//Rutas de alumnos
Route::get('alumnos', [
    'as' => 'alumnos',
    'uses' => 'ControladorAlumno@index'
])->middleware("log");
Route::get('alumnos/borrar/{id}', [
    'as' => 'alumnos.destroy',
    'uses' => 'ControladorAlumno@destroy'
])->middleware("log");
Route::get('alumnos/editar/{id}', [
    'as' => 'alumnos.edit',
    'uses' => 'ControladorAlumno@edit'
])->middleware("log");
Route::get('alumnos/crear', [
    'as' => 'alumnos.create',
    'uses' => 'ControladorAlumno@create'
])->middleware("log");
Route::get('alumnos/guardar', [
    'as' => 'alumnos.store',
    'uses' => 'ControladorAlumno@store'
])->middleware("log");
Route::POST('alumnos/actualizar/{id}', [
    'as' => 'alumnos.update',
    'uses' => 'ControladorAlumno@update'
])->middleware("log");
Route::get('alumnos/buscar', [
    'as' => 'alumnos.show',
    'uses' => 'ControladorAlumno@show'
])->middleware("log");