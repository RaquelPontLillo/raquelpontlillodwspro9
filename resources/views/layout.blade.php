<!DOCTYPE html>
<html lang="es">
    <head>
        <title>@yield('titulo', 'Matrículas') | Matrículas App. 2016-2017</title>
        <link rel="shortcut icon" href="{{ asset('images/kandel.ico') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <meta charset="UTF-8">
        <style>
            body {
                background: url("{{ asset('images/fondo.png') }}"), repeat;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button> 
                            <a class="navbar-brand" href="{{route('/')}}"><img src="{{ asset('images/kandel.ico') }}" /></a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{route('/')}}"><span class="glyphicon glyphicon-home"></span> Inicio</a></li>
                                <li class="dropdown">
                                    <a href="{{route('cursos')}}" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-book"></span> Gestión de cursos<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('cursos')}}"><span class="glyphicon glyphicon-eye-open"></span> Ver todos</a></li>
                                        <li><a href="{{route('cursos.create')}}"><span class="glyphicon glyphicon-plus"></span> Crear nuevo</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="{{route('alumnos')}}" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Gestión de alumnos<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('alumnos')}}"><span class="glyphicon glyphicon-eye-open"></span> Ver todos</a></li>
                                        <li><a href="{{route('alumnos.create')}}"><span class="glyphicon glyphicon-plus"></span> Crear nuevo</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="{{route('logout')}}"><span class="glyphicon glyphicon-off"></span> Cerrar sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    
                    @if (session()->has('flash_notification.message'))
                        <div class="alert alert-{{ session('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {!! session('flash_notification.message') !!}
                        </div>
                    @endif

                    <div class="jumbotron">
                        @yield('contenido')
                    </div>

                    <footer>
                        <pre>Provided by KandelSoft. <span class="pull-right">Raquel Pont Lillo</span></pre>
                    </footer>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>