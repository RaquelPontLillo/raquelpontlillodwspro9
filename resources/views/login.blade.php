<!DOCTYPE html>
<html lang="es">
<head>
    <title>Control de acceso | Matrículas App. 2016-2017</title>
    <link rel="shortcut icon" href="{{ asset('images/kandel.ico') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <meta charset="UTF-8">
    <style>
    body {
        background: url("{{ asset('images/fondo.png') }}"), repeat;
    }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            @if (session()->has('flash_notification.message'))
            <br />
            <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                <div class="alert alert-{{ session('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! session('flash_notification.message') !!}
                </div>
            </div>
            @endif
            <br />

            <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                <div class="jumbotron">
                    <form class="form-signin" method="POST" action="{{ route('autenticar') }}">
                        <h2 class="form-signin-heading"><span class="glyphicon glyphicon-lock"></span> Acceso restringido: identifícate</h2>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                        <label for="user" class="sr-only">Usuario</label>
                        <input type="text" id="user" name="usuario" class="form-control" placeholder="Nombre de usuario" value="root" required autofocus>
                        <label for="inputPassword" class="sr-only">Contraseña</label>
                        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" value="pass" required>
                        <button class="btn btn-lg btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-log-in"></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>