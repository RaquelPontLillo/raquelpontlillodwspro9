@extends('layout')

@section('titulo', 'Inicio')

@section('contenido')
    <h3><span class="glyphicon glyphicon-education"></span> Bienvenido a «Matrículas App. 2016-2017»</h3>
<br />
<h4><span class="glyphicon glyphicon-folder-open"></span>  Documentación del proyecto en PDF</h4>

<div class="embed-responsive embed-responsive-4by3">
  <embed class="embed-responsive-item" src="{{ asset('docs/documentacion.pdf') }}" type="application/pdf"></embed>
</div>
@endsection