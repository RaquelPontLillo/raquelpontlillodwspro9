@extends('layout')

@section('titulo', 'Ver cursos')

@section('contenido')
    <h3><span class="glyphicon glyphicon-eye-open"></span> Ver lista de cursos</h3>
    <div class="row">
        <div class="col-md-6">
            <a href="{{route('cursos.create')}}" class="btn btn-info" data-toggle="tooltip" title="Crear curso"><span class="glyphicon glyphicon-plus"></span></a>
        </div>
        <div class="col-md-6">
            <form action="{{route('cursos.show')}}" method="get" class="pull-right">
                <div class="input-group">
                    <input name="id" type="text" class="form-control" placeholder="Buscar por ID">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Nombre</th>
        <th>Horas</th>
        <th>Acción</th>
    </thead>
    <tbody>
        @foreach ($cursos as $curso)
            <tr>
                <td>{{ $curso->id }}</td>
                <td>{{ $curso->nombre }}</td>
                <td>{{ $curso->horas }}</td>
                <td>
                    @if (count($cursos) != 0)
                        <a href="{{route('cursos.edit', $curso->id)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar curso"><span class="glyphicon glyphicon-pencil"></span></a>
                        <span data-toggle="modal" data-target="#borrar-curso-{{ $curso->id }}">
                            <a href="#borrar-curso-{{ $curso->id }}" data-toggle="tooltip" title="Borrar curso" class="btn btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </span>
                        <div class="modal fade" id="borrar-curso-{{ $curso->id }}" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Borrar curso «{{ $curso->nombre }}»</h4>
                                    </div>
                                    <div class="modal-body">
                                        ¿Quieres borrar este curso? Esta acción no puede deshacerse, y <span class="text-danger">también eliminará a aquellos alumnos asociados al curso</span>.
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{route('cursos.destroy', $curso->id)}}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
    @if (count($cursos) > 1)
        <div class="pull-right">{{ $cursos->links() }}</div>
    @endif
    <br/>
@endsection