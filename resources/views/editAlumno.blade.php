@extends('layout')

@section('titulo', 'Editar alumno')

@section('contenido')
    <h3><span class="glyphicon glyphicon-pencil"></span> Editar alumno «{{ $alumno->nombre }}»</h3>

    <form action="{{ route('alumnos.update', $alumno->id) }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <div class="form-group">
            <label name="id">ID</label>
            <input name="id" type="text" class="form-control" readonly="readonly" value="{{ $alumno->id }}" />
        </div>
        
        <div class="form-group">
            <label name="nombre">Nombre</label>
            <input name="nombre" type="text" class="form-control" placeholder="Título del curso" value="{{ $alumno->nombre }}" />
        </div>

        <div class="form-group">
            <label name="curso">Curso</label>
            <select name="curso" class="form-control">
                <option value="0">-- Escoge un curso --</option>
                @foreach ($cursos as $curso)
                    @if ($alumno->curso_id == $curso->id)        
                        <option value="{{ $curso->id }}" selected>
                    @else
                        <option value="{{ $curso->id }}">
                    @endif
                        {{ $curso->nombre }}
                        ({{ $curso->horas }}h)
                        </option>
                @endforeach;  
            </select>
    </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
        </div>
    </form>
@endsection