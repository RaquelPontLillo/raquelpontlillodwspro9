@extends('layout')

@section('titulo', 'Ver alumnos')

@section('contenido')
    <h3><span class="glyphicon glyphicon-eye-open"></span> Ver lista de alumnos</h3>
        <div class="row">
            <div class="col-md-6">
                <a href="{{route('alumnos.create')}}" class="btn btn-info" data-toggle="tooltip" title="Crear alumno"><span class="glyphicon glyphicon-plus"></span></a>
            </div>
            <div class="col-md-6">
                <form action="{{route('alumnos.show')}}" method="get" class="pull-right">
                    <div class="input-group">
                        <input name="id" type="text" class="form-control" placeholder="Buscar por ID">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    <table class="table table-striped">
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Curso</th>
            <th>Acción</th>
        </thead>
        <tbody>

        @foreach ($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->id }}</td>
                <td>{{ $alumno->nombre }}</td>
                <td>{{ $alumno->cursos->nombre }}</td>
            <td>
                    @if (count($alumnos) != 0)
                        <a href="{{route('alumnos.edit', $alumno->id)}}" class="btn btn-warning" data-toggle="tooltip" title="Editar alumno"><span class="glyphicon glyphicon-pencil"></span></a>
                        <span data-toggle="modal" data-target="#borrar-alumno-{{ $alumno->id }}">
                            <a href="#borrar-alumno-{{ $alumno->id }}" data-toggle="tooltip" title="Borrar alumno" class="btn btn-danger">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </span>
                        <div class="modal fade" id="borrar-alumno-{{ $alumno->id }}" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="myModalLabel">Borrar alumno «{{ $alumno->nombre }}»</h4>
                                    </div>
                                    <div class="modal-body">
                                        ¿Quieres borrar este alumno? Esta acción no puede deshacerse.
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{route('alumnos.destroy', $alumno->id)}}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (count($alumnos) > 1)
        <div class="pull-right">{{ $alumnos->links() }}</div>
    @endif
    <br/>
@endsection