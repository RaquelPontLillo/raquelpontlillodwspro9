@extends('layout')

@section('titulo', 'Crear alumno')

@section('contenido')
    <h3><span class="glyphicon glyphicon-plus"></span> Crear alumno</h3>

    <form action="{{ route('alumnos.store') }}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <div class="form-group">
            <label name="id">ID</label>
            <input name="id" type="text" class="form-control" readonly="readonly" value="{{ $id }}" />
        </div>
        
        <div class="form-group">
            <label name="nombre">Nombre</label>
            <input name="nombre" type="text" class="form-control" placeholder="Nombre del alumno" value="" />
        </div>

        <div class="form-group">
            <label name="curso_id">Curso</label>
            <select name="curso_id" class="form-control">
                <option value="0">-- Escoge un curso --</option>
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}">
                        {{ $curso->nombre }}
                        ({{ $curso->horas }}h)
                    </option>
                @endforeach;  
            </select>
    </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
        </div>
    </form>
@endsection