@extends('layout')

@section('titulo', 'Crear curso')

@section('contenido')
    <h3><span class="glyphicon glyphicon-plus"></span> Crear nuevo curso</h3>

    <form action="{{route('cursos.store')}}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
        <div class="form-group">
            <label name="id">ID</label>
            <input name="id" type="text" class="form-control" readonly="readonly" value="{{ $id }}" />
        </div>
        
        <div class="form-group">
            <label name="nombre">Nombre</label>
            <input name="nombre" type="text" class="form-control" placeholder="Título del curso" />
        </div>

        <div class="form-group">
            <label name="horas">Horas</label>
            <input name="horas" type="text" class="form-control" placeholder="Horas totales" />
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
        </div>
    </form>
@endsection